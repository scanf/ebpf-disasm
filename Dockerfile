FROM jimmycuadra/rust

WORKDIR /tmp

RUN cargo install --git https://github.com/badboy/ebpf-disasm
ENTRYPOINT ["/root/.cargo/bin/ebpf-disasm"]
# docker run -v ${PWD}:/tmp scanf/ebpf-disasm -s classifier /tmp/bpf.o
